﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Net;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Utility;
using uPLibrary.Networking.M2Mqtt.Exceptions;

using System;

public class mqttTest : MonoBehaviour {


	public Image ledBttn;

	private Text textLed;
	private bool ligado = false;

	private MqttClient client;
	// Use this for initialization
	void Start () {
		// create client instance 
		client = new MqttClient(IPAddress.Parse("35.156.233.3"),1883 , false , null ); 
		
		// register to message received 
		client.MqttMsgPublishReceived += client_MqttMsgPublishReceived; 
		
		string clientId = Guid.NewGuid().ToString(); 
		client.Connect(clientId); 
		
		// subscribe to the topic "/home/temperature" with QoS 2 
		client.Subscribe(new string[] { "080996/outTopic" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE }); 


		textLed = ledBttn.transform.Find ("Text").GetComponent<Text> ();

	}
	void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e) 
	{ 

		Debug.Log("Received: " + System.Text.Encoding.UTF8.GetString(e.Message)  );
	} 


	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.L)) {
			OnOff ();
		}

		if (Input.GetButtonDown ("Fire2")) {
			client.Publish("080996/inTopic", System.Text.Encoding.UTF8.GetBytes("ON"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
			textLed.text = "Led On";
			ledBttn.color = Color.green;

		}

		if (Input.GetButtonUp ("Fire2")) {
			client.Publish("080996/inTopic", System.Text.Encoding.UTF8.GetBytes("OFF"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
			textLed.text = "Led Off";
			ledBttn.color = Color.red;
		}


	}


	public void OnOff ()
	{
		if (ligado) {
			ligado = false;
			client.Publish("080996/inTopic", System.Text.Encoding.UTF8.GetBytes("OFF"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
			textLed.text = "Led Off";
			ledBttn.color = Color.red;


		} else {
			ligado = true;
			textLed.text = "Led On";
			client.Publish("080996/inTopic", System.Text.Encoding.UTF8.GetBytes("ON"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
			ledBttn.color = Color.green;
		}
	}
}
