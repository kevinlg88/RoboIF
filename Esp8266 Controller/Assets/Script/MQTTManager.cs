﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Net;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Utility;
using uPLibrary.Networking.M2Mqtt.Exceptions;

using System;

public class MQTTManager : MonoBehaviour {

	//HUD
	public Image upArrow;
	public Image upRightArrow;
	public Image upLeftArrow;
	public Image downArrow;
	public Image leftArrow;
	public Image rightArrow;
	public Image downRightArrow;
	public Image downLeftArrow;
	public Image center;

	//States
	private bool frente = false;
	private bool tras = false;
	private bool esquerda = false;
	private bool direita = false;
	private bool parado = true;

	private MqttClient client;
	// Use this for initialization
	void Start () {
		// create client instance 
		client = new MqttClient(IPAddress.Parse("35.156.233.3"),1883 , false , null ); 

		// register to message received 
		client.MqttMsgPublishReceived += client_MqttMsgPublishReceived; 

		string clientId = Guid.NewGuid().ToString(); 
		client.Connect(clientId); 

		// subscribe to the topic "/home/temperature" with QoS 2 
		client.Subscribe(new string[] { "080996/outTopic" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE }); 



	}
	void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e) 
	{ 

		Debug.Log("Received: " + System.Text.Encoding.UTF8.GetString(e.Message)  );
	} 


	// Update is called once per frame
	void Update () {

		float move = Input.GetAxis("Horizontal");
		if (move > 0.8) {
			direita = true;

			esquerda = false;
			parado = false;

		} else if (move < -0.8) {
			esquerda = true;

			direita = false;
			parado = false;

		} else if (move > -0.8 && move < 0.8 && !frente && !tras){
			parado = true;

			frente = false;
			tras = false;
			esquerda = false;
			direita = false;
		}





		// A button down
		if (Input.GetButtonDown ("Frente") && !direita && !esquerda) {
			frente = true;

			tras = false;
			esquerda = false;
			direita = false;
			parado = false;
		}
		if (Input.GetButtonDown ("Frente") && direita) {
			frente = true;
			direita = true;

			tras = false;
			esquerda = false;
			parado = false;
		}
		if (Input.GetButtonDown ("Frente") && esquerda) {
			frente = true;
			esquerda = true;

			tras = false;
			direita = false;
			parado = false;
		}






		// X button down
		if (Input.GetButtonDown ("Tras") && !direita && !esquerda) {
			tras = true;

			frente = false;
			esquerda = false;
			direita = false;
			parado = false;
		}

		if (Input.GetButtonDown ("Tras") && direita) {
			tras = true;
			direita = true;

			frente = false;
			esquerda = false;
			parado = false;
		}

		if (Input.GetButtonDown ("Tras") && esquerda) {
			tras = true;
			esquerda = true;

			frente = false;
			direita = false;
			parado = false;
		}







		// A button up
		if (Input.GetButtonUp ("Frente")) {
			frente = false;
		}

		// X button up
		if (Input.GetButtonUp ("Tras")) {
			tras = false;
		}

		HudManager ();

	}



	private void HudManager()
	{
		if (frente && !esquerda && !direita) {
			upArrow.color = Color.green;
			client.Publish("080996/inTopic", System.Text.Encoding.UTF8.GetBytes("f"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
		} else {
			upArrow.color = Color.white;
		}

		if (frente && esquerda) {
			upLeftArrow.color = Color.green;
			client.Publish("080996/inTopic", System.Text.Encoding.UTF8.GetBytes("ef"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
		} else {
			upLeftArrow.color = Color.white;
		}

		if (frente && direita) {
			upRightArrow.color = Color.green;
			client.Publish("080996/inTopic", System.Text.Encoding.UTF8.GetBytes("df"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
		} else {
			upRightArrow.color = Color.white;
		}





		if (esquerda && !frente && !tras) {
			leftArrow.color = Color.green;
			client.Publish ("080996/inTopic", System.Text.Encoding.UTF8.GetBytes ("e"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
		} else {
			leftArrow.color = Color.white;
		}

		if (direita && !frente && !tras) {
			rightArrow.color = Color.green;
			client.Publish ("080996/inTopic", System.Text.Encoding.UTF8.GetBytes ("d"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
		} else {
			rightArrow.color = Color.white;
		}






		if (tras && !esquerda && !direita) {
			downArrow.color = Color.green;
			client.Publish("080996/inTopic", System.Text.Encoding.UTF8.GetBytes("t"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
		} else {
			downArrow.color = Color.white;
		}

		if (tras && esquerda) {
			downLeftArrow.color = Color.green;
			client.Publish("080996/inTopic", System.Text.Encoding.UTF8.GetBytes("et"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
		} else {
			downLeftArrow.color = Color.white;
		}

		if (tras && direita) {
			downRightArrow.color = Color.green;
			client.Publish("080996/inTopic", System.Text.Encoding.UTF8.GetBytes("dt"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
		} else {
			downRightArrow.color = Color.white;
		}





		if (parado) {
			center.color = Color.green;
			client.Publish("080996/inTopic", System.Text.Encoding.UTF8.GetBytes("p"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
		} else {
			center.color = Color.white;
		}
	}
		
}